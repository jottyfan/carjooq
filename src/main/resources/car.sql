SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;
SET default_tablespace = '';
SET default_table_access_method = heap;

CREATE TABLE public.t_mileage (
    pk integer generated always as identity primary key,
    mileage numeric(7,0),
    fuel text,
    location text,
    price numeric(5,2),
    amount numeric(5,2),
    provider text,
    buydate timestamp without time zone,
    annotation text
);

CREATE VIEW public.v_mileage AS
SELECT pk,
  mileage,
  fuel,
  location,
  price,
  amount,
  provider,
  buydate,
  ((price / amount))::numeric(4,2) AS "euro/l",
  annotation
FROM public.t_mileage;

COPY public.t_mileage (pk, mileage, fuel, location, price, amount, provider, buydate, annotation) FROM stdin;
-2	123456	E10	Berlin	14.32	10.01	Agip	2000-12-31 16:32:00
-1	124012	E10	Potsdam	59.18	49.73	BP	2001-02-24 13:14:00	
0	124680	Super	Eberswalde	24.42	16.43	SB	2001-03-14 08:19:00	
\.

GRANT USAGE ON SCHEMA public TO jooq;
GRANT SELECT ON TABLE public.t_mileage TO jooq;
GRANT SELECT ON TABLE public.v_mileage TO jooq;
